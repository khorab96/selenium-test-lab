import Pages.LoginPage;
import config.Config;
import org.testng.annotations.Test;

import java.util.UUID;

public class Lab_18_Test_Tworzenia_Charakterystyki_Test extends SeleniumBaseTest {

    @Test
    public void addCharacteristicTest ( ) {
        String processName = "DEMO PROJECT";
        String characteristicName = UUID.randomUUID ( ).toString ( ).substring ( 0, 10 );
        String lsl = "8";
        String usl = "10";
        new LoginPage ( driver )
                .typeEmail ( new Config ( ).getApplicationUser ( ) )
                .typePassword ( new Config ( ).getApplicationPassword ( ) )
                .submitLogin ( )
                .goToCharacteristics ( )
                .assertIfCharacteristicsPageIsOpen ( )
                .goToCreateCharacteristicPage ( )
                .selectProcess ( processName )
                .typeName ( characteristicName )
                .typeLsl ( lsl )
                .typeUsL ( usl )
                .successfulSubmit ( )
                .assertCharacteristic ( characteristicName, lsl, usl, "" );


    }

}
