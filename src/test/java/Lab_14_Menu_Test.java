import Pages.LoginPage;
import config.Config;
import org.testng.annotations.Test;

public class Lab_14_Menu_Test extends SeleniumBaseTest {

    @Test
    public void goToProcesses ( ) {
        new LoginPage ( driver )
                .typeEmail ( new Config ( ).getApplicationUser ( ) )
                .typePassword ( new Config ( ).getApplicationPassword ( ) )
                .submitLogin ( )
                .goToProcesses ( )
                .assertIfProcessesPageIsOpen ( )
                .goToCharacteristics ( )
                .assertIfCharacteristicsPageIsOpen ( )
                .goToHomePage ( )
                .assertIfHomePageIsOpen ( );
    }

}
