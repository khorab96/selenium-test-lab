import Pages.LoginPage;
import config.Config;
import org.testng.annotations.Test;

import java.util.UUID;

public class Lab_19_Test_Tworzenia_Charakterystyki_Negatywny_Test extends SeleniumBaseTest {

    @Test
    public void incorrectCharacteristicCreationTest ( ) {
        String expMsg = "The value 'Select process' is not valid for ProjectId.";
        String characteristicName = UUID.randomUUID ( ).toString ( ).substring ( 0, 10 );
        String lsl = "8";
        String usl = "10";
        new LoginPage ( driver )
                .typeEmail ( new Config ( ).getApplicationUser ( ) )
                .typePassword ( new Config ( ).getApplicationPassword ( ) )
                .submitLogin ( )
                .goToCharacteristics ( )
                .assertIfCharacteristicsPageIsOpen ( )
                .goToCreateCharacteristicPage ( )
                .typeName ( characteristicName )
                .typeLsl ( lsl )
                .typeUsL ( usl )
                .submitWithFailure ( )
                .assertProjectErrorMsgAppears ( expMsg )
                .backToCharacteristicsPage ( )
                .assertCharacteristicIsNotShown ( characteristicName );
    }


}
