import Pages.LoginPage;
import config.Config;
import org.testng.annotations.Test;

import java.util.UUID;

public class Lab_20_Test_Raportu extends SeleniumBaseTest {

    @Test
    public void reportTest ( ) {

        String processName = "DEMO PROJECT";
        String characteristicName = UUID.randomUUID ( ).toString ( ).substring ( 0, 10 );
        String lsl = "8";
        String usl = "10";
        String sampleName = "Test sample";
        String results = "8.0;9.0";
        String expMean = "8.5000";
        new LoginPage ( driver )
                .typeEmail ( new Config ( ).getApplicationUser ( ) )
                .typePassword ( new Config ( ).getApplicationPassword ( ) )
                .submitLogin ( )
                .goToCharacteristics ( )
                .goToCreateCharacteristicPage ( )
                .selectProcess ( processName )
                .typeName ( characteristicName )
                .typeLsl ( lsl )
                .typeUsL ( usl )
                .successfulSubmit ( )
                .gotToResults ( characteristicName )
                .addSampleResults ( )
                .typeSampleName ( sampleName )
                .typeResults ( results )
                .successfullyAddResults ( )
                .backToCharacteristicsPage ( )
                .goToReport ( characteristicName )
                .assertMean( expMean );


    }
}
