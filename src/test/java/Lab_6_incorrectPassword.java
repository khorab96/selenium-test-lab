import config.Config;
import org.junit.Ignore;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;
@Ignore
public class Lab_6_incorrectPassword extends SeleniumBaseTest{
    @Test
    public void incorrectLoginTestWrongEmail ( ) {

        WebElement emailAddressFld = driver.findElement ( By.cssSelector ( "#Email" ) );
        emailAddressFld.sendKeys ( new Config ( ).getApplicationUser ( ) );

        WebElement passwordFld = driver.findElement ( By.cssSelector ( "#Password" ) );
        passwordFld.sendKeys ( "Test1!1111111" );

        WebElement loginBtn = driver.findElement ( By.cssSelector ( "button[type=submit]" ) );
        loginBtn.click ( );


        List<WebElement> validationErrors = driver.findElements ( By.cssSelector ( ".validation-summary-errors>ul>li" ) );
        boolean doesErrorExists = false;

        for (int i = 0; i < validationErrors.size ( ); i++) {
            if (validationErrors.get ( i ).getText ( ).equals ( "Invalid login attempt." )) {
                doesErrorExists = true;
                break;
            }
        }
        Assert.assertTrue ( doesErrorExists );

        doesErrorExists = validationErrors
                .stream ( )
                .anyMatch ( validationError -> validationError.getText ( ).equals ( "Invalid login attempt." ) );
        Assert.assertTrue ( doesErrorExists );


        driver.quit ( );

    }
}
