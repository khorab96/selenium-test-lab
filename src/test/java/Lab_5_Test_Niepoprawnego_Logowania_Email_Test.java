import config.Config;
import org.junit.Ignore;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;
@Ignore
public class Lab_5_Test_Niepoprawnego_Logowania_Email_Test extends SeleniumBaseTest {

    @Test
    public void incorrectLoginTestWrongEmail ( ) {

        WebElement emailAddressFld = driver.findElement ( By.cssSelector ( "#Email" ) );
        emailAddressFld.sendKeys ( "test1t" );

        WebElement passwordFld = driver.findElement ( By.cssSelector ( "#Password" ) );
        passwordFld.sendKeys ( new Config ( ).getApplicationPassword ( ) );

        WebElement loginBtn = driver.findElement ( By.cssSelector ( "button[type=submit]" ) );
        loginBtn.click ( );

        WebElement errorMsg = driver.findElement ( By.cssSelector ( "#Email-error" ) );
        Assert.assertTrue ( errorMsg.getText ( ).contains ( "The Email field is not a valid e-mail address" ) );

        List<WebElement> validationErrors = driver.findElements ( By.cssSelector ( ".validation-summary-errors>ul>li" ) );
        boolean doesErrorExists = false;

        for (int i = 0; i < validationErrors.size ( ); i++) {
            if (validationErrors.get ( i ).getText ( ).equals ( "The Email field is not a valid e-mail address." )) {
                doesErrorExists = true;
                break;
            }
        }
        Assert.assertTrue ( doesErrorExists );

        doesErrorExists = validationErrors
                .stream ( )
                .anyMatch ( validationError -> validationError.getText ( ).equals ( "The Email field is not a valid e-mail address." ) );
        Assert.assertTrue ( doesErrorExists );


        driver.quit ( );


    }


}
