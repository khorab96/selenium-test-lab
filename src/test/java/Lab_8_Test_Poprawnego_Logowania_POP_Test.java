import Pages.LoginPage;
import config.Config;
import org.junit.Ignore;
import org.testng.annotations.Test;
@Ignore
public class Lab_8_Test_Poprawnego_Logowania_POP_Test extends SeleniumBaseTest {

    @Test
    public void correctLoginTest ( ) {

        new LoginPage ( driver )
                .typeEmail ( new Config ( ).getApplicationUser ( ) )
                .typePassword ( new Config ( ).getApplicationPassword ( ) )
                .submitLogin ( )
                .assertWelcomeElmIsShown ( );


    }


}
