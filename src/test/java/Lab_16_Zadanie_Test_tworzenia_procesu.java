import Pages.LoginPage;
import config.Config;
import org.testng.annotations.Test;

import java.util.UUID;

public class Lab_16_Zadanie_Test_tworzenia_procesu extends SeleniumBaseTest {

    String processName = UUID.randomUUID ( ).toString ( ).substring ( 0, 10 );

    @Test
    public void addProcessTest ( ) {
        new LoginPage ( driver )
                .typeEmail ( new Config ( ).getApplicationUser ( ) )
                .typePassword ( new Config ( ).getApplicationPassword ( ) )
                .submitLogin ( )
                .goToProcesses ( )
                .assertIfProcessesPageIsOpen ( )
                .goToCreateProcessPage ( )
                .typeProcessName ( processName )
                .typeProcessDescription ( "test" )
                .typeProcessNotes ( "test" )
                .submitProcessSuccess ( )
                .assertProcess ( processName, "test", "test" );
    }

}
