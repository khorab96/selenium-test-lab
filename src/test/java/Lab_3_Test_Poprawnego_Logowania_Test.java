import config.Config;
import org.junit.Ignore;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;
@Ignore
public class Lab_3_Test_Poprawnego_Logowania_Test extends SeleniumBaseTest {

    @Test
    public void correctLoginTest (){

        WebElement emailAddressFld = driver.findElement ( By.cssSelector ( "#Email" ));
        emailAddressFld.sendKeys (new Config().getApplicationUser () );

        WebElement passwordFld = driver.findElement ( By.cssSelector (  "#Password") );
        passwordFld.sendKeys ( new Config ( ).getApplicationPassword ( )  );

        WebElement loginBtn = driver.findElement ( By.cssSelector ( "button[type=submit]" ) );
        loginBtn.click ( );

        WebElement welcomeElm = driver.findElement(By.cssSelector(".profile_info>h2"));
        Assert.assertTrue ( welcomeElm.getText ().contains ( "Welcome" ) );

        driver.quit ();







    }
}
