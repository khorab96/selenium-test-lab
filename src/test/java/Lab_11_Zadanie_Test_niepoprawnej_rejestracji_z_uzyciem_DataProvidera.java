import Pages.LoginPage;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Lab_11_Zadanie_Test_niepoprawnej_rejestracji_z_uzyciem_DataProvidera extends SeleniumBaseTest {


    @DataProvider
    public Object[][] getWrongEmails ( ) {
        return new Object[][]{
                {"test"},
                {"admin"},
                {"@test"}
        };
    }

    @Test(dataProvider = "getWrongEmails")
    public void incorrectRegistration ( String wrongEmails ) {

        new LoginPage ( driver )
                .clickRegisterLink ( )
                .typeEmail ( wrongEmails )
                .typePassword ( "rwrwr11" )
                .typeConfirmPassword ( "rwrwr11" )
                .clickRegisterBtnWithFailure ( )
                .assertEmailErrorIsShown ( );
    }

}
