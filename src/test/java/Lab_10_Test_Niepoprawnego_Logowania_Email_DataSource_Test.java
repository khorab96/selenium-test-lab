import Pages.LoginPage;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Lab_10_Test_Niepoprawnego_Logowania_Email_DataSource_Test extends SeleniumBaseTest {


    @DataProvider
    public Object[][] getWrongEmails ( ) {
        return new Object[][]{
                {"test"},
                {"admin"},
                {"@test"}
        };
    }

    @Test(dataProvider = "getWrongEmails")
    public void incorrectEmailTest ( String wrongEmail ) {

        new LoginPage ( driver )
                .typeEmail ( wrongEmail )
                .submitLoginWithFailure ( )
                .assertEmailErrorMsgIsShown ( );

    }

}
