import Pages.LoginPage;
import config.Config;
import org.junit.Ignore;
import org.testng.annotations.Test;
@Ignore
public class Lab_9_Test_Niepoprawnego_Logowania extends SeleniumBaseTest {


    @Test
    public void incorrectLoginTest ( ) {

        LoginPage loginPage = new LoginPage ( driver )
                .typeEmail ( "test1@test.com" )
                .typePassword ( new Config ( ).getApplicationPassword ( ) )
                .submitLoginWithFailure ( )
                .assertLoginErrorMsgIsShown ( );
    }
}
