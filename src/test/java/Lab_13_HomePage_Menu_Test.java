import Pages.LoginPage;
import config.Config;
import org.testng.annotations.Test;

public class Lab_13_HomePage_Menu_Test extends SeleniumBaseTest {

    @Test
    public void goToProcesses ( ) {
        new LoginPage ( driver )
                .typeEmail ( new Config ( ).getApplicationUser ( ) )
                .typePassword ( new Config ( ).getApplicationPassword ( ) )
                .submitLogin ( )
                .goToProcesses ( );
    }

    @Test
    public void goToCharacteristics ( ) {
        new LoginPage ( driver )
                .typeEmail ( new Config ( ).getApplicationUser ( ) )
                .typePassword ( new Config ( ).getApplicationPassword ( ) )
                .submitLogin ( )
                .goToCharacteristics ( );
    }

    @Test
    public void goToHomePage ( ) {
        new LoginPage ( driver )
                .typeEmail ( new Config ( ).getApplicationUser ( ) )
                .typePassword ( new Config ( ).getApplicationPassword ( ) )
                .submitLogin ( )
                .goToHomePage ( );
    }

}
