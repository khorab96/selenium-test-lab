package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CreateResultsPage extends ResultsPage {
    public CreateResultsPage( WebDriver driver ){
        super(driver);
    }

    @FindBy(id = "Sample")
    private WebElement sampleNameTxt;

    @FindBy(id = "Values")
    private WebElement resultsTxt;

    @FindBy(css = "input[value*='Create']")
    public WebElement createBtn;

    public CreateResultsPage typeSampleName(String sampleName){
        sampleNameTxt.clear ();
        sampleNameTxt.sendKeys ( sampleName );
        return this;
    }

    public CreateResultsPage typeResults(String results){
        resultsTxt.clear ();
        resultsTxt.sendKeys ( results );
        return this;
    }

    public ResultsPage successfullyAddResults(){
        WebDriverWait wait = new WebDriverWait ( driver, 5 );
        wait.until ( ExpectedConditions.elementToBeClickable ( createBtn ) );
        createBtn.click ();
        return new ResultsPage ( driver );
    }


}
