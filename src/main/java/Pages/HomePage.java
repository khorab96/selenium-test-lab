package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class HomePage {
    protected WebDriver driver;

    public HomePage ( WebDriver driver ) {
        this.driver = driver;
        PageFactory.initElements ( driver, this );
    }

    @FindBy(css = ".profile_info>h2")
    private WebElement welcomeElm;

    @FindBy(css = ".menu-workspace")
    private WebElement workspaceNav;

    @FindBy(css = "a[href$=Projects]")
    private WebElement processesMenu;

    @FindBy(css = "a[href$=Characteristics]")
    private WebElement characteristicsMenu;

    @FindBy(css = " li.active > ul > li > a")
    private WebElement dashboardMenu;

    @FindBy(css = ".x_title")
    private WebElement demoProjectTitle;

    @FindBy(css = ".menu-home")
    private WebElement homeNav;

    public boolean isParentExpanded ( WebElement menuLink ) {
        WebElement parent = menuLink.findElement ( By.xpath ( "./.." ) );
        return parent.getAttribute ( "class" ).contains ( "active" );
    }

    public ProcessesPage goToProcesses ( ) {
        if (!isParentExpanded ( workspaceNav )) {
            workspaceNav.click ( );
        }
        WebDriverWait wait = new WebDriverWait ( driver, 5 );
        wait.until ( ExpectedConditions.elementToBeClickable ( processesMenu ) );
        processesMenu.click ( );
        return new ProcessesPage ( driver );
    }

    public CharacteristicsPage goToCharacteristics ( ) {
        if (!isParentExpanded ( workspaceNav )) {
            workspaceNav.click ( );
        }
        WebDriverWait wait = new WebDriverWait ( driver, 5 );
        wait.until ( ExpectedConditions.elementToBeClickable ( characteristicsMenu ) );
        characteristicsMenu.click ( );
        return new CharacteristicsPage ( driver );
    }

    public HomePage goToHomePage ( ) {
        if (!isParentExpanded ( homeNav )) {
            homeNav.click ( );
        }
        WebDriverWait wait = new WebDriverWait ( driver, 5 );
        wait.until ( ExpectedConditions.elementToBeClickable ( dashboardMenu ) );
        dashboardMenu.click ( );
        return this;
    }


    public HomePage assertWelcomeElmIsShown ( ) {
        Assert.assertTrue ( welcomeElm.isDisplayed ( ), "Welcome element is not shown" );
        Assert.assertTrue ( welcomeElm.getText ( ).contains ( "Welcome" ), "Welcome element text is not displayed" );
      return this;
    }

    public HomePage assertIfHomePageIsOpen ( ) {
        Assert.assertEquals ( driver.getCurrentUrl ( ), "http://qaaghspclab.northeurope.cloudapp.azure.com/", "this is not a home page" );
        Assert.assertTrue ( demoProjectTitle.getText ( ).contains ( "DEMO PROJECT" ), "this is not a home page" );
        return this;
    }
}
