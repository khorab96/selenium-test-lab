package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class CreateCharacteristicPage extends CharacteristicsPage {
    public CreateCharacteristicPage ( WebDriver driver ) {
        super ( driver );
    }

    @FindBy(id = "ProjectId")
    private WebElement projectSlc;

    @FindBy(id = "Name")
    private WebElement characteristicNameTxt;

    @FindBy(id = "LowerSpecificationLimit")
    private WebElement lowerSpecLimitTxt;

    @FindBy(id = "UpperSpecificationLimit")
    private WebElement upperSpecLimitTxt;

    @FindBy(css = "input[type*='Submit']")
    private WebElement submitBtn;

    @FindBy(css ="[data-valmsg-for*='ProjectId']")
    private WebElement selectErrorMsg;

    @FindBy(css = "div>a[href*='/Characteristics']")
    private WebElement backToCharacteristics;

    public CreateCharacteristicPage typeName ( String name ) {
        characteristicNameTxt.clear ( );
        characteristicNameTxt.sendKeys ( name );
        return this;
    }

    public CreateCharacteristicPage typeLsl ( String lowerSpecLimit ) {
        lowerSpecLimitTxt.clear ( );
        lowerSpecLimitTxt.sendKeys ( lowerSpecLimit );
        return this;
    }

    public CreateCharacteristicPage typeUsL ( String upperSpecLimit ) {
        upperSpecLimitTxt.clear ( );
        upperSpecLimitTxt.sendKeys ( upperSpecLimit );
        return this;
    }

    public CreateCharacteristicPage selectProcess ( String processName ) {
        new Select ( projectSlc ).selectByVisibleText ( processName );
        return this;
    }

    public CharacteristicsPage successfulSubmit ( ) {
        submitBtn.click ( );
        return new CharacteristicsPage ( driver );
    }

    public CreateCharacteristicPage submitWithFailure(){
        submitBtn.click ();
        return this;
    }

    public CreateCharacteristicPage assertProjectErrorMsgAppears(String expMsg){
        Assert.assertTrue ( selectErrorMsg.getText ().contains ( expMsg ) );
        return this;
    }

    public CharacteristicsPage backToCharacteristicsPage(){
        backToCharacteristics.click ();
        return new CharacteristicsPage ( driver );
    }
}
