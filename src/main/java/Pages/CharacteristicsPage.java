package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.List;

public class CharacteristicsPage extends HomePage {


    public CharacteristicsPage ( WebDriver driver ) {
        super ( driver );
    }

    @FindBy(css = ".title_left")
    private WebElement characteristicsPageTitle;

    @FindBy(css = "a[href*='/Characteristics/Create']")
    private WebElement createCharacteristicBtn;




    public CreateCharacteristicPage goToCreateCharacteristicPage ( ) {
        createCharacteristicBtn.click ( );
        return new CreateCharacteristicPage ( driver );
    }

    public ResultsPage gotToResults(String characteristicName){
        String resultsBtnXpath = String.format(GENERIC_CHARACTERISTIC_RESULTS_XPATH, characteristicName);
        driver.findElement(By.xpath(resultsBtnXpath)).click();

        return new ResultsPage(driver);
    }
    private String GENERIC_CHARACTERISTIC_REPORT_XPATH = "//td[text()='%s']/..//a[contains(@href, 'Report')]";

    public ReportPage goToReport(String characteristicName){
        String reportBtnXpath = String.format(GENERIC_CHARACTERISTIC_REPORT_XPATH, characteristicName);
        driver.findElement(By.xpath(reportBtnXpath)).click();

        return new ReportPage(driver);
    }
    private String GENERIC_CHARACTERISTIC_ROW_XPATH = "//td[text()='%s']/..";

    public CharacteristicsPage assertCharacteristic ( String expName, String expLsl, String expUsl, String expBinCount ) {
        String characteristicXpath = String.format ( GENERIC_CHARACTERISTIC_ROW_XPATH, expName );
        WebElement characteristicRow = driver.findElement ( By.xpath ( characteristicXpath ) );

        String actLsl = characteristicRow.findElement ( By.xpath ( "./td[3]" ) ).getText ( );
        String actUsl = characteristicRow.findElement ( By.xpath ( "./td[4]" ) ).getText ( );
        String actBinCount = characteristicRow.findElement ( By.xpath ( "./td[5]" ) ).getText ( );

        Assert.assertEquals ( actLsl, expLsl );
        Assert.assertEquals ( actUsl, expUsl );
        Assert.assertEquals ( actBinCount, expBinCount );

        return this;
    }

    public CharacteristicsPage assertCharacteristicIsNotShown(String characteristicName) {
        String characteristicXpath = String.format(GENERIC_CHARACTERISTIC_ROW_XPATH, characteristicName);
        List<WebElement> process = driver.findElements(By.xpath(characteristicXpath));
        Assert.assertEquals(process.size(), 0);
        return this;
    }
    private String GENERIC_CHARACTERISTIC_RESULTS_XPATH = "//td[text()='%s']/..//a[contains(@href, 'Results')]";


    public CharacteristicsPage assertIfCharacteristicsPageIsOpen ( ) {
        Assert.assertEquals ( driver.getCurrentUrl ( ), "http://qaaghspclab.northeurope.cloudapp.azure.com/Characteristics", "this is not a characteristics page" );
        Assert.assertTrue ( characteristicsPageTitle.getText ( ).contains ( "Characteristics" ), "this is not a characteristics page" );
        return this;
    }

}