package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ResultsPage extends CharacteristicsPage {

    public ResultsPage( WebDriver driver ){
        super(driver);
    }

    @FindBy(css = "a.btn.btn-success")
    private WebElement  addResultsBtn;

    @FindBy(css = "div>a[href*='/Characteristics']")
    private WebElement backToCharBtn;


    public CreateResultsPage addSampleResults(){
        addResultsBtn.click ();
        return new CreateResultsPage(driver);
    }

    public CharacteristicsPage backToCharacteristicsPage(){
        backToCharBtn.click ();
        return new CharacteristicsPage ( driver );
    }

}
