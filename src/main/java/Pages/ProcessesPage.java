package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.List;

public class ProcessesPage extends HomePage {


    public ProcessesPage ( WebDriver driver ) {
        super ( driver );
    }

    @FindBy(css = ".title_left")
    private WebElement processesPageHeaderTitle;


    @FindBy(css = "a[href*='/Projects/Create']")
    private WebElement addNewProcessBtn;


    public ProcessesPage assertIfProcessesPageIsOpen ( ) {

        Assert.assertEquals ( driver.getCurrentUrl ( ), "http://qaaghspclab.northeurope.cloudapp.azure.com/Projects", "this is not a processes page" );
        Assert.assertTrue ( processesPageHeaderTitle.getText ( ).contains ( "Processes" ), "this is not a processes page" );
        return this;
    }

    private String GENERIC_PROCESS_ROW_XPATH = "//td[text()='%s']/..";

    public ProcessesPage assertProcess ( String expName, String expDescription, String expNotes ) {
        String processXpath = String.format ( GENERIC_PROCESS_ROW_XPATH, expName );

        WebElement processRow = driver.findElement ( By.xpath ( processXpath ) );

        String actDescription = processRow.findElement ( By.xpath ( "./td[2]" ) ).getText ( );
        String actNotes = processRow.findElement ( By.xpath ( "./td[3]" ) ).getText ( );

        Assert.assertEquals ( actDescription, expDescription );
        Assert.assertEquals ( actNotes, expNotes );

        return this;
    }

    public CreateProcessPage goToCreateProcessPage ( ) {
        addNewProcessBtn.click ( );
        return new CreateProcessPage ( driver );
    }

    public ProcessesPage assertProcessIsNotShown(String processName) {
        String processXpath = String.format(GENERIC_PROCESS_ROW_XPATH, processName);
        List<WebElement> process = driver.findElements(By.xpath(processXpath));
        Assert.assertEquals(process.size(), 0);
        return this;
    }

}
